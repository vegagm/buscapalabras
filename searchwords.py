#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''


import sys
import sortwords

def search_word(word, words_list):
    word_in_list: bool = False
    for i in range(0, len(words_list) - 1):
        if word.lower() == words_list[i].lower():
            test_index = i
            word_in_list = True

    if word_in_list:
        return test_index
    else:
        print("no esta")
        raise Exception

def main():
    if len(sys.argv) >= 2:
        word = sys.argv[1]
        words_list = sys.argv[2:]
    else:
        sys.exit("No se ha encontrado ningún argumento.")


    ordered_list: list = sortwords.sort(words_list)
    sortwords.show(ordered_list)

    try:
        print(search_word(word, ordered_list))
    except:
        sys.exit("eeror excepcion")


if __name__ == '__main__':
    main()
