#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import searchwords

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

class TestSearchWord(unittest.TestCase):

    def test_simple(self):
        position = searchwords.search_word('hola', ['hola', 'adios'])
        self.assertEqual(0 , position)

    def test_simple2(self):
        position = searchwords.search_word('hola', ['adios', 'bien', 'hola', 'mal'])
        self.assertEqual(2 , position)

    def test_long(self):
        position = searchwords.search_word('ddd', ['aaa', 'bbb', 'ccc', 'ddd', 'eee',
                                                   'fff', 'ggg', 'hhh', 'iii', '́jj]'])
        self.assertEqual(3, position)

    def test_upper(self):
        position = searchwords.search_word('dDd', ['aaa', 'Bbb', 'ccc', 'Ddd', 'eee',
                                                   'fff', 'ggG', 'hhh', 'Iii', '́jj]'])
        self.assertEqual(3, position)

    def test_except(self):
        with patch.object(sys, 'argv', ['searchwords.py']):
            with self.assertRaises(Exception):
                position = searchwords.search_word('xxx', ['aaa', 'Bbb', 'ccc', 'Ddd', 'eee',
                                                       'fff', 'ggG', 'hhh', 'Iii', '́jj]'])

class TestMain(unittest.TestCase):

    def test_simple(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['searchwords.py', 'hola', 'hola', 'adios']):
            with contextlib.redirect_stdout(stdout):
                searchwords.main()
        output = stdout.getvalue()
        self.assertEqual('adios hola \n1\n' , output)

    def test_simple2(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['searchwords.py', 'hola', 'bien', 'mal', 'hola', 'adios']):
            with contextlib.redirect_stdout(stdout):
                searchwords.main()
        output = stdout.getvalue()
        self.assertEqual('adios bien hola mal \n2\n' , output)

    def test_long(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['searchwords.py', 'hola', 'bien', 'mal', 'hola', 'adios',
                                        'first', 'second', 'third', 'fourth', 'malo', 'peor']):
            with contextlib.redirect_stdout(stdout):
                searchwords.main()
        output = stdout.getvalue()
        self.assertEqual('adios bien first fourth hola mal malo peor second third \n4\n' , output)

    def test_one(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['searchwords.py', 'hola', 'hola']):
            with contextlib.redirect_stdout(stdout):
                searchwords.main()
        output = stdout.getvalue()
        self.assertEqual('hola \n0\n' , output)

    def test_upper(self):
        stdout = StringIO()
        with patch.object(sys, 'argv', ['searchwords.py', 'Hola', 'bien', 'mal', 'hoLa', 'aDios',
                                        'first', 'second', 'third', 'fourth', 'Malo', 'peoR']):
            with contextlib.redirect_stdout(stdout):
                searchwords.main()
        output = stdout.getvalue()
        self.assertEqual('aDios bien first fourth hoLa mal Malo peoR second third \n4\n' , output)

    def test_few_args(self):
        with patch.object(sys, 'argv', ['searchwords.py', 'XXX']):
            with self.assertRaises(SystemExit) as context:
                searchwords.main()

    def test_except(self):
        with patch.object(sys, 'argv', ['searchwords.py', 'XXX', 'bien', 'mal', 'hoLa', 'aDios',
                                        'first', 'second', 'third', 'fourth', 'Malo', 'peoR']):
            with self.assertRaises(SystemExit) as context:
                searchwords.main()


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
